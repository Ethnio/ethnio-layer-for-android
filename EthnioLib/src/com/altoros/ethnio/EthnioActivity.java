package com.altoros.ethnio;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.altoros.ethnio.EthnioClient.OnEthnioCloseClickListener;

/**
 * <h1>EthnioActivity</h1><br>
 * Activity with Ethnio content. For displaying this activity please use
 * EthnioManager's methods:
 * <ul>
 * <li>{@link EthnioManager#startEthnioActivity(int)}</li>
 * <li>{@link EthnioManager#startEthnioActivityIfAvailable(int)}</li>
 * </ul>
 * 
 */
public class EthnioActivity extends Activity implements OnEthnioCloseClickListener {

	static final String EXTRA_MAIN_URL = "MAIN_URL";
	static final String EXTRA_ETHNIO_ID = "ETHNIO_ID";
	static final String EXTRA_LISTENER_ID = "LISTENER_ID";

	private int mListenerId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getIntent() == null) {
			showErrorToast();
			finish();
		}

		String mainUrl = getIntent().getStringExtra(EXTRA_MAIN_URL);
		String ethnioId = getIntent().getStringExtra(EXTRA_ETHNIO_ID);
		mListenerId = getIntent().getIntExtra(EXTRA_LISTENER_ID, 0);
		if (TextUtils.isEmpty(mainUrl) || TextUtils.isEmpty(ethnioId)) {
			showErrorToast();
			finish();
		}

		setContentView(EthnioManager.createEthnioView(this, mainUrl, ethnioId, this, mListenerId));
	}

	private void showErrorToast() {
		Toast.makeText(this, "For displaying this activity please use EthnioManager", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onCloseClick() {
		finish();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent intent = new Intent(EthnioManager.ACTION_ETHNIO_CLIENT_CLICK_CLOSE);
		intent.putExtra(EthnioManager.EXTRA_ETHNIO_LISTENER_ID_EXTRA, mListenerId);
		sendBroadcast(intent);
	}
}
