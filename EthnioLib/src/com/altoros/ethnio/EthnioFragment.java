package com.altoros.ethnio;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * <h1>EthnioFragment</h1><br>
 * Fragment with Ethnio content. For creation this fragment please use
 * EthnioManager's methods:
 * <ul>
 * <li>{@link EthnioManager#createEthnioFragment(int)}</li>
 * <li>{@link EthnioManager#createEthnioFragment(EthnioEventListener, int)}</li>
 * <li>{@link EthnioManager#createEthnioFragmentIfAvailable(int)}</li>
 * </ul>
 * 
 */
public class EthnioFragment extends Fragment {

	public static final String EXTRA_MAIN_URL = "MAIN_URL";
	public static final String EXTRA_ETHNIO_ID = "ETHNIO_ID";
	public static final String EXTRA_LISTENER_ID = "LISTENER_ID";

	public static EthnioFragment newInstance(String mainUrl, String ethnioId, int listenerId) {
		Bundle args = new Bundle();
		args.putString(EXTRA_MAIN_URL, mainUrl);
		args.putString(EXTRA_ETHNIO_ID, ethnioId);
		args.putInt(EXTRA_LISTENER_ID, listenerId);
		EthnioFragment fragment = new EthnioFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (getArguments() == null) {
			showErrorToast();
		}

		String mainUrl = getArguments().getString(EXTRA_MAIN_URL);
		String ethnioId = getArguments().getString(EXTRA_ETHNIO_ID);
		int listenerId = getArguments().getInt(EXTRA_LISTENER_ID);
		if (TextUtils.isEmpty(mainUrl) || TextUtils.isEmpty(ethnioId)) {
			showErrorToast();
		}
		return EthnioManager.createEthnioView(getActivity(), mainUrl, ethnioId, null, listenerId);
	}

	private void showErrorToast() {
		Toast.makeText(getActivity(), "For creation this fragment please use EthnioManager", Toast.LENGTH_SHORT).show();
	}

}
