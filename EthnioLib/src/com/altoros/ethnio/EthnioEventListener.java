package com.altoros.ethnio;

import android.support.v4.app.Fragment;

/**
 * <h1>EthnioEventListener</h1><br>
 * Interface definition for callbacks to be invoked when Ethnio processed.
 * Extands {@link EthnioAvailabilityListener}
 */
public interface EthnioEventListener extends EthnioAvailabilityListener {

	/**
	 * Called when Ethnio activity has been started
	 * 
	 * @param listenerId
	 *            Id of called listener
	 */
	public void onEthnioActivityStarted(int listenerId);

	/**
	 * Called when Ethnio fragment has been created
	 * 
	 * @param fragment
	 *            Fragment with Ethnio content
	 * @param listenerId
	 *            Id of called listener
	 */
	public void onEthnioFragmentCreated(Fragment fragment, int listenerId);

	/**
	 * Called when Ethnio content has been loaded
	 * 
	 * @param listenerId
	 *            Id of called listener
	 */
	public void onEthnioLoaded(int listenerId);

	/**
	 * Called when Ethnio Close button has been clicked
	 * 
	 * @param listenerId
	 *            Id of called listener
	 */
	public void onEthnioCloseClick(int listenerId);

}
