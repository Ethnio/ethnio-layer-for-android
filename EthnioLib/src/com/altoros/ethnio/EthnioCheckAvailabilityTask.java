package com.altoros.ethnio;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

class EthnioCheckAvailabilityTask extends AsyncTask<String, Void, String> {

	// URL for checking Ethnio availability
	private static final String CHECK_AVAILABILITY_URL = "is_active/";
	// Name of JSON field with code
	private static final String JSON_FIELD_CODE = "id";

	// Queue for checking duplication of Ethnio
	private EthnioQueue mQueue;
	// Listener for Ethnio availability events
	private EthnioAvailabilityListener mAvailabilityListener;
	private int mListenerId;

	public EthnioCheckAvailabilityTask(Context context, EthnioAvailabilityListener availabilityListener, int listenerId) {
		mQueue = new EthnioQueue(context);
		mAvailabilityListener = availabilityListener;
		mListenerId = listenerId;
	}

	@Override
	protected String doInBackground(String... params) {
		try {
			URL url = new URL(params[0] + CHECK_AVAILABILITY_URL + params[1]);
			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
			connection.setHostnameVerifier(new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			});
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String jsonString = bufferedReader.readLine();
			if (!TextUtils.isEmpty(jsonString)) {
				JSONObject jsonObject = new JSONObject(jsonString);
				return jsonObject.getString(JSON_FIELD_CODE);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	};

	protected void onPostExecute(String result) {
		if (mAvailabilityListener != null) {
			if (TextUtils.isEmpty(result)) {
				mAvailabilityListener.onEthnioLoadError(mListenerId);
			} else if (result.equals("0")) {
				mAvailabilityListener.onEthnioUnavailable(mListenerId);
			} else {
				if (mQueue.putValue(result)) {
					mAvailabilityListener.onEthnioAvailable(mListenerId);
				} else {
					mAvailabilityListener.onEthnioDuplicated(mListenerId);
				}
			}
		}
	};

	@Override
	protected void onCancelled() {
		if (mAvailabilityListener != null) {
			mAvailabilityListener.onEthnioUnavailable(mListenerId);
		}
	}
}