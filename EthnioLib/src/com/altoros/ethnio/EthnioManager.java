package com.altoros.ethnio;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;

import com.altoros.ethnio.EthnioClient.OnEthnioCloseClickListener;

/**
 * <h1>EthnioManager</h1><br>
 * Check Ethnio availability, start {@link EthnioActivity} and create
 * {@link EthnioFragment} with Ethnio content. Use next methods:
 * <ul>
 * <li>
 * {@link EthnioManager#checkEthnioAvailability(EthnioAvailabilityListener, int)}
 * </li>
 * <li>{@link EthnioManager#startEthnioActivity(int)}</li>
 * <li>{@link EthnioManager#startEthnioActivityIfAvailable(int)}</li>
 * <li>{@link EthnioManager#createEthnioFragment(int)}</li>
 * <li>{@link EthnioManager#createEthnioFragment(EthnioEventListener, int)}</li>
 * <li>{@link EthnioManager#createEthnioFragmentIfAvailable(int)}</li>
 * </ul>
 * <b>Note:</b><br>
 * You should call {@link EthnioManager#stop()} in <code>onDestroy</code> of
 * Activity, where <code>EthnioManager</code> will been created
 */

public class EthnioManager {

	static final String MAIN_URL = "https://new.ethn.io/";
	static final String ACTION_ETHNIO_CLIENT_CLICK_CLOSE = "com.altoros.ethnio.ethniomanager.ACTION_ETHNIO_CLIENT_CLICK_CLOSE";
	static final String ACTION_ETHNIO_LOADED = "com.altoros.ethnio.ethniomanager.ACTION_ETHNIO_LOADED";
	static final String EXTRA_ETHNIO_LISTENER_ID_EXTRA = "ETHNIO_LISTENER_ID";
	static final String OPEN_URL = "mob/";

	private Context mContext;
	private String mEthnioId;
	private String mMainUrl = MAIN_URL;
	private EthnioEventListener mEventListener;
	private EthnioCheckAvailabilityTask mCheckEthnioAvailabilityTask;

	private BroadcastReceiver mClientEventReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null && !TextUtils.isEmpty(intent.getAction()) && mEventListener != null
					&& intent.getExtras() != null) {
				int listenerId = intent.getIntExtra(EXTRA_ETHNIO_LISTENER_ID_EXTRA, 0);
				if (intent.getAction().equals(ACTION_ETHNIO_CLIENT_CLICK_CLOSE)) {
					mEventListener.onEthnioCloseClick(listenerId);
				} else if (intent.getAction().equals(ACTION_ETHNIO_LOADED)) {
					mEventListener.onEthnioLoaded(listenerId);
				}
			}
		}
	};

	/**
	 * Initialize {@link EthnioManager} with <code>default</code> Ethnio server
	 * Url
	 * 
	 * @param context
	 *            Context of application or called activity
	 * @param ethnioId
	 *            Id key for Ethnio
	 * @param eventListener
	 *            Listener for callbacks to be invoked when Ethnio processed
	 */
	public EthnioManager(Context context, String ethnioId, EthnioEventListener eventListener) {
		mContext = context;
		mEthnioId = ethnioId;
		mEventListener = eventListener;
		registerClientEventReceiver();
	}

	/**
	 * Initialize {@link EthnioManager} with <code>custom</code> Ethnio server
	 * Url
	 * 
	 * @param context
	 *            Context of application or called activity
	 * @param ethnioId
	 *            Id key for Ethnio
	 * @param mainUrl
	 *            Url of Ethnio server
	 * @param eventListener
	 *            Listener for callbacks to be invoked when Ethnio processed
	 */
	public EthnioManager(Context context, String ethnioId, String mainUrl, EthnioEventListener eventListener) {
		mContext = context;
		mEthnioId = ethnioId;
		mMainUrl = mainUrl;
		mEventListener = eventListener;
		registerClientEventReceiver();
	}

	private void registerClientEventReceiver() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(ACTION_ETHNIO_CLIENT_CLICK_CLOSE);
		filter.addAction(ACTION_ETHNIO_LOADED);
		mContext.registerReceiver(mClientEventReceiver, filter);
	}

	/**
	 * Stop {@link EthnioManager} and all {@link EthnioEventListener}s. Should
	 * be called in <code>onDestroy</code> of Activity, where
	 * <code>EthnioManager</code> has been created
	 */
	public void stop() {
		mContext.unregisterReceiver(mClientEventReceiver);
	}

	/**
	 * Check Ethnio availability and (if available) start {@link EthnioActivity}
	 * with Ethnio content and callbacks to {@link EthnioEventListener} with
	 * <code>listenerId</code>
	 * 
	 * @param listenerId
	 *            Id of called listener
	 */
	public void startEthnioActivityIfAvailable(final int listenerId) {
		checkEthnioAvailability(new EthnioAvailabilityListenerWrapper(mEventListener) {
			@Override
			public void onEthnioAvailable(int id) {
				super.onEthnioAvailable(id);
				startEthnioActivity(listenerId);
			}
		}, listenerId);
	}

	/**
	 * Start {@link EthnioActivity} with Ethnio content and callbacks to
	 * {@link EthnioEventListener} with <code>listenerId</code>. Don't check
	 * Ethnio availability
	 * 
	 * @param listenerId
	 *            Id of called listener
	 */
	public void startEthnioActivity(int listenerId) {
		Intent intent = new Intent(mContext, EthnioActivity.class);
		intent.putExtra(EthnioActivity.EXTRA_MAIN_URL, mMainUrl);
		intent.putExtra(EthnioActivity.EXTRA_ETHNIO_ID, mEthnioId);
		intent.putExtra(EthnioActivity.EXTRA_LISTENER_ID, listenerId);
		mContext.startActivity(intent);
		if (mEventListener != null) {
			mEventListener.onEthnioActivityStarted(listenerId);
		}
	}

	/**
	 * Check Ethnio availability and (if available) create
	 * {@link EthnioFragment} with Ethnio content and return it in
	 * <code>eventListener</code> with <code>listenerId</code>
	 * 
	 * @param listenerId
	 *            Id of called listener where <code>EthnioFragment</code> will
	 *            be returned
	 */
	public void createEthnioFragmentIfAvailable(final int listenerId) {
		checkEthnioAvailability(new EthnioAvailabilityListenerWrapper(mEventListener) {
			@Override
			public void onEthnioAvailable(int id) {
				super.onEthnioAvailable(id);
				createEthnioFragment(mEventListener, listenerId);
			}
		}, listenerId);
	}

	/**
	 * Create {@link EthnioFragment} with Ethnio content and return it in
	 * <code>eventListener</code>. Don't check Ethnio availability
	 * 
	 * @param eventListener
	 *            Listener for callbacks to be invoked when Ethnio processed.
	 *            <code>EthnioFragment</code> will be returned here
	 * @param listenerId
	 *            Id of called listener
	 */
	public void createEthnioFragment(EthnioEventListener eventListener, int listenerId) {
		if (mEventListener != null) {
			mEventListener.onEthnioFragmentCreated(createEthnioFragment(listenerId), listenerId);
		}
	}

	/**
	 * Create {@link EthnioFragment} with Ethnio content and return it here.
	 * Don't check Ethnio availability
	 * 
	 * @param listenerId
	 *            Id of called listener
	 * @return Fragment with Ethnio content
	 */
	public Fragment createEthnioFragment(int listenerId) {
		return EthnioFragment.newInstance(mMainUrl, mEthnioId, listenerId);
	}

	/**
	 * Check Ethnio availability
	 * 
	 * @param availabilityListener
	 *            Listener with callbaks to be invoked when Ethnio availability
	 *            is checked
	 * @param listenerId
	 *            Id of called listener
	 */
	public void checkEthnioAvailability(EthnioAvailabilityListener availabilityListener, int listenerId) {
		if (isOnline(mContext)) {
			if (mCheckEthnioAvailabilityTask != null) {
				mCheckEthnioAvailabilityTask.cancel(false);
			}
			mCheckEthnioAvailabilityTask = new EthnioCheckAvailabilityTask(mContext, availabilityListener, listenerId);
			mCheckEthnioAvailabilityTask.execute(mMainUrl, mEthnioId);
		} else {
			if (availabilityListener != null) {
				availabilityListener.onInternetUnavailable(listenerId);
			}
		}
	}

	@SuppressLint("SetJavaScriptEnabled")
	static View createEthnioView(Context context, String mainUrl, String ethnioId,
			OnEthnioCloseClickListener closeClickListener, int listenerId) {
		WebView view = new WebView(context);
		view.setId(1);
		view.setWebViewClient(new EthnioClient(closeClickListener, listenerId));
		view.getSettings().setJavaScriptEnabled(true);
		view.loadUrl(mainUrl + OPEN_URL + ethnioId);
		return view;
	}

	/**
	 * Check Internet connectivity
	 * 
	 * @param context
	 *            Context with ConnectivityManager
	 * @return <code>true</code> if Internet is accessible, else
	 *         <code>false</code>
	 */
	public boolean isOnline(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

}
