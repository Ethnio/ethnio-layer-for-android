package com.altoros.ethnio;

/**
 * <h1>EthnioAvailabilityListener</h1><br>
 * Interface definition for callbacks to be invoked when Ethnio availability is
 * checked
 */
public interface EthnioAvailabilityListener {

	/**
	 * Called if Internet connection is unavailable
	 * 
	 * @param listenerId
	 *            Id of called listener
	 */
	public void onInternetUnavailable(int listenerId);

	/**
	 * Called if load error occurred
	 * 
	 * @param listenerId
	 *            Id of called listener
	 */
	public void onEthnioLoadError(int listenerId);

	/**
	 * Called if Ethnio service is unavailable or returned ZERO code
	 * 
	 * @param listenerId
	 *            Id of called listener
	 */
	public void onEthnioUnavailable(int listenerId);

	/**
	 * Called if Ethnio service is available and returned VALID code
	 * 
	 * @param listenerId
	 *            Id of called listener
	 */
	public void onEthnioAvailable(int listenerId);

	/**
	 * Called if Ethnio service is available and returned DUPLICATED code
	 * 
	 * @param listenerId
	 *            Id of called listener
	 */
	public void onEthnioDuplicated(int listenerId);

}
