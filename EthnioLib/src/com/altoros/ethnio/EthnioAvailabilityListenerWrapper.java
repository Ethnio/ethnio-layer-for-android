package com.altoros.ethnio;

/**
 * <h1>EthnioAvailabilityListenerWrapper</h1><br>
 * This class based on {@link EthnioAvailabilityListener} and wraps callbacks to
 * be invoked when Ethnio availability is checked
 */
public class EthnioAvailabilityListenerWrapper implements EthnioAvailabilityListener {

	private EthnioAvailabilityListener mAvailabilityListener;

	public EthnioAvailabilityListenerWrapper(EthnioAvailabilityListener availabilityListener) {
		mAvailabilityListener = availabilityListener;
	}

	@Override
	public void onInternetUnavailable(int listenerId) {
		if (mAvailabilityListener != null) {
			mAvailabilityListener.onInternetUnavailable(listenerId);
		}
	}

	@Override
	public void onEthnioUnavailable(int listenerId) {
		if (mAvailabilityListener != null) {
			mAvailabilityListener.onEthnioUnavailable(listenerId);
		}
	}

	@Override
	public void onEthnioLoadError(int listenerId) {
		if (mAvailabilityListener != null) {
			mAvailabilityListener.onEthnioLoadError(listenerId);
		}
	}

	@Override
	public void onEthnioDuplicated(int listenerId) {
		if (mAvailabilityListener != null) {
			mAvailabilityListener.onEthnioDuplicated(listenerId);
		}
	}

	@Override
	public void onEthnioAvailable(int listenerId) {
		if (mAvailabilityListener != null) {
			mAvailabilityListener.onEthnioAvailable(listenerId);
		}
	}
}
