package com.altoros.ethnio;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.Queue;

import android.content.Context;

class EthnioQueue {

	private static final String FILE_NAME = "com.altoros.ethnio.queue";
	private static final int QUEUE_LENGTH = 10;

	private Context mContext;

	public EthnioQueue(Context context) {
		mContext = context;
	}

	/**
	 * Try to put ID in queue and return TRUE if queue does not contain this ID
	 * or FALSE in otherwise.
	 * 
	 * @param ethnioId
	 *            - ID for putting in queue
	 * @return TRUE in success, FALSE if queue contains ID
	 */
	@SuppressWarnings("unused")
	public boolean putValue(String code) {
		synchronized (EthnioQueue.class) {
			Queue<String> queue = loadQueue();

			if (QUEUE_LENGTH == 0) {
				// ZERO LENGTH without check
				return true;
			}

			if (queue.contains(code)) {
				queue.remove(code);
				queue.add(code);
				saveQueue(queue);
				return false;
			} else {
				if (queue.size() >= QUEUE_LENGTH) {
					queue.poll();
				}
				queue.add(code);
				saveQueue(queue);
				return true;
			}
		}
	}

	@SuppressWarnings("unchecked")
	private Queue<String> loadQueue() {
		Queue<String> queue = null;
		try {
			ObjectInputStream reader = new ObjectInputStream(mContext.openFileInput(FILE_NAME));
			queue = (Queue<String>) reader.readObject();
			reader.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			queue = new LinkedList<String>();
		}
		return queue;
	}

	private void saveQueue(Queue<String> queue) {
		try {
			ObjectOutputStream writer = new ObjectOutputStream(mContext.openFileOutput(FILE_NAME, Context.MODE_PRIVATE));
			writer.writeObject(queue);
			writer.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
