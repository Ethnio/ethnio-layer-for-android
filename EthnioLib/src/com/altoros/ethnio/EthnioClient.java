package com.altoros.ethnio;

import android.content.Context;
import android.content.Intent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class EthnioClient extends WebViewClient {

	// Link, click by that will call onClientClickClose of EthnioEventListener
	private static final String URL_CLOSE = "app://close";

	private OnEthnioCloseClickListener mCloseClickListener;
	private int mListenerId;

	public EthnioClient(OnEthnioCloseClickListener closeClickListener, int listenerId) {
		mCloseClickListener = closeClickListener;
		mListenerId = listenerId;
	}

	@Override
	public boolean shouldOverrideUrlLoading(WebView view, String url) {
		if (url.equals(URL_CLOSE)) {
			sendBroadcast(view.getContext(), EthnioManager.ACTION_ETHNIO_CLIENT_CLICK_CLOSE);
			if (mCloseClickListener != null) {
				mCloseClickListener.onCloseClick();
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void onPageFinished(WebView view, String url) {
		super.onPageFinished(view, url);
		sendBroadcast(view.getContext(), EthnioManager.ACTION_ETHNIO_LOADED);
	}

	private void sendBroadcast(Context context, String action) {
		Intent intent = new Intent(action);
		intent.putExtra(EthnioManager.EXTRA_ETHNIO_LISTENER_ID_EXTRA, mListenerId);
		context.sendBroadcast(intent);
	}

	static interface OnEthnioCloseClickListener {
		public void onCloseClick();
	}

}
