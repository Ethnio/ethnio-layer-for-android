package com.altoros.ethniodemo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.altoros.ethnio.EthnioEventListener;
import com.altoros.ethnio.EthnioManager;

public class MainActivity extends FragmentActivity implements OnClickListener {

	private static final String ETHNIO_ID = "62935";

	private EthnioManager mEthnioManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mEthnioManager = new EthnioManager(this, ETHNIO_ID, mEthnioEventListener);

		findViewById(R.id.open_frame_button).setOnClickListener(this);
		findViewById(R.id.open_activity_button).setOnClickListener(this);
		findViewById(R.id.check_availability_button).setOnClickListener(this);
		findViewById(R.id.check_and_open_frame_button).setOnClickListener(this);
		findViewById(R.id.check_and_open_activity_button).setOnClickListener(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mEthnioManager.stop();
	}

	private EthnioEventListener mEthnioEventListener = new EthnioEventListener() {

		@Override
		public void onInternetUnavailable(int listenerId) {
			Toast.makeText(MainActivity.this, listenerId + ": Internet Unavailable", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onEthnioLoadError(int listenerId) {
			Toast.makeText(MainActivity.this, listenerId + ": Load Error", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onEthnioUnavailable(int listenerId) {
			Toast.makeText(MainActivity.this, listenerId + ": Ethnio Unavailable", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onEthnioAvailable(int listenerId) {
			Toast.makeText(MainActivity.this, listenerId + ": Ethnio Avaliable", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onEthnioDuplicated(int listenerId) {
			Toast.makeText(MainActivity.this, listenerId + ": Ethnio Duplicated", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onEthnioActivityStarted(int listenerId) {
			Toast.makeText(MainActivity.this, listenerId + ": Ethnio Activity started", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onEthnioFragmentCreated(Fragment fragment, int listenerId) {
			FragmentTransaction fTrans = getSupportFragmentManager().beginTransaction();
			fTrans.replace(R.id.frame, fragment);
			fTrans.commit();
			Toast.makeText(MainActivity.this, listenerId + ": Ethnio fragment created", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onEthnioLoaded(int listenerId) {
			Toast.makeText(MainActivity.this, listenerId + ": Ethnio Loaded", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onEthnioCloseClick(int listenerId) {
			Toast.makeText(MainActivity.this, listenerId + ": Ethnio Close click", Toast.LENGTH_SHORT).show();
		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.open_frame_button:
			mEthnioManager.createEthnioFragment(mEthnioEventListener, 1);
			break;
		case R.id.open_activity_button:
			mEthnioManager.startEthnioActivity(2);
			break;
		case R.id.check_availability_button:
			mEthnioManager.checkEthnioAvailability(mEthnioEventListener, 3);
			break;
		case R.id.check_and_open_frame_button:
			mEthnioManager.createEthnioFragmentIfAvailable(4);
			break;
		case R.id.check_and_open_activity_button:
			mEthnioManager.startEthnioActivityIfAvailable(5);
			break;
		}
	}
}
