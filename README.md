EthnioLib
===============
EthnioLib is an android library (project) with `EthnioManager` for requests to display an ethnio screener that an existing ethnio creates using the ethnio web app. 

Supports all screen sizes and orientations.
EthnioLib works starting with android 8 API.

Source
-------------
* `EthnioDemo` - folder with source code of EthnioDemo project
* `EthnioLib` - folder with source code of EthnioLib
* `ethniolib.jar` - latest version of EthnioLib

Usage
-------------
EthnioLib can open activity or create a fragment with Ethnio content.

* First of all add EthnioLib.jar to lib folder of your project or add EthnioLib project as a dependency for your project.
* Get ScrennerAppId from the server and save it as a constant for future use.
```java
private static final String ETHNIO_ID = "11111"; //example code
```
* Implement instance of EthnioEventLIstener for callbacks.
```java
private EthnioEventListener mEthnioEventListener = new EthnioEventListener() {}
```
* Initialize instance of EthnioManager in onCreate method of activity.
```java
mEthnioManager = new EthnioManager(this, ETHNIO_ID, mEthnioEventListener);
```
* Stop EthnioManager in onDestory method of activity.
```java
mEthnioManager.stop();
```
* For showing activity with Ethnio content, register EthnioActivity in AndroidManifest.xml of your project.
```xml
<activity android:name="com.altoros.ethnio.EthnioActivity" />
```
And use the method of EthnioManagers instance for showing it (use code for the difference listeners).
```java
mEthnioManager.startEthnioActivityIfAvailable(777);
```
Activity will appear if Ethnio is available or else send callback with event.instance of EthnioManager as field of activity and initialize it in onCreate method of activity.

* For creation of a fragment with Ethnio content use the method of EthnioManagers instance.
```java
mEthnioManager.createEthnioFragmentIfAvailable(4);
```
And show fragment from EthnioEventListeners callback.
```java
@Override
public void onEthnioFragmentCreated(Fragment fragment, int listenerId) {
FragmentTransaction fTrans = 
getSupportFragmentManager().beginTransaction();
	fTrans.replace(R.id.frame, fragment);
	fTrans.commit();
	Toast.makeText(MainActivity.this, listenerId + ": Ethnio fragment created", Toast.LENGTH_SHORT).show();
}
```

For more examples, including how to use EthnioLib, take a look at the `EthnioDemo` project.

Consulting
-------------
If you need additional support or help integrating and/or customizing the controller for your project, feel free to get [help@ethn.io](mailto:help@ethn.io)